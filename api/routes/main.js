"use strict"

const fs = require('fs');
const moment = require('moment');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var _ = require('../../api/tools/utilities.js');
var $ = require('../../api/tools/nifty.js');

module.exports = function(app, database) {
  var accounts = database.accounts;


  function scrubPasswords(arr){
    for(var i in arr){
      delete arr[i].pass;
    }
    return arr;
  }

  app.get('/', function(req, res){
    var data = {user:{username:"guest"}};
    if(req.session.user){
      data = {user:req.session.user};
    }
    res.render('home/index', {data: data});
  });


  app.get('/friendlist', function(req, res) {

      accounts.find({conversations:req.session.user.username}).toArray(function(e, o){
        if(o){
          res.send(scrubPasswords(o));
        } else {
          res.send(400);
        }
      });
  });


  app.post('/friendlist', function(req, res) {
      accounts.findOne({username:req.session.user.username }, function(e, o){
        if(o){
          accounts.findOne({id:req.body.id }, function(e, x){
            if(x){
              if(o.conversations.indexOf(x.username) === -1){
                o.conversations.push(x.username);
              }
              if(x.conversations.indexOf(req.session.user.username) === -1){
                x.conversations.push(req.session.user.username);
              }
              accounts.save(o, function(err) {
                  req.session.user = o;
                  accounts.save(x, function(err) {
                      res.send(200);
                  }, res);
              }, res);
            } else {
              res.send(400);
            }
          });
        } else {
          res.send(400);
        }
      });
  });

  app.get('/logout', function(req, res) {
    delete req.session.user;
    res.redirect("/");
  });

  function findSpecific(data, find, origin){

    let sendData = [];
    for(var i in data){
      if((data[i].username).toLowerCase().indexOf(find.toLowerCase()) !== -1 && data[i].username !== origin){
        delete data[i].pass;
        sendData.push(data[i]);
      }
    }
    return sendData;
  }

  app.get('/userlist', function(req, res) {

    if(req.query.check === "one"){
      accounts.findOne({username:req.query.username}, function(e, o){
        if(o){
          res.send(true);
        } else {
          res.send(false);
        }
      });
    } else {
      accounts.find({}).toArray(function(e, o){
        if(o){

          var find = (req.query.username || "");
          var origin = req.session.user.username;
          
          res.send((findSpecific(o, find, origin)));
        } else {
          res.send(400);
        }
      });
    }
  });

  app.post('/login', function(req, res) {

        if(req.body.username && req.body.pass){
            var login = {};
            login.username = _.htmlEscape(req.body.username).toLowerCase();
            login.pass = _.htmlEscape(req.body.pass);

            if(login.username && login.pass){
                accounts.findOne({username:login.username}, function(e, o) {
                    if (!o){
                        res.send(400);

                    } else {
                        _.validatePassword(login.pass, o.pass, function(err, tet) {
                            if (tet){
                                o.lastlogin = moment().format('MMMM Do YYYY, h:mm:ss a');
                                accounts.save(o, function(err) {
                                    req.session.user = o;
                                    res.send(o, 200);
                                }, res);
                            } else {

                                res.send(400); //Your password or username is incorrect.
                            }
                        });
                    }
                });
            } else {

                res.send(400);
            }
        } else {

			res.send(400);
		}
	});

  app.post('/signup', function(req, res){
      //get signup information

      if(req.body.pass && req.body.username){
          var signup = {};
              signup.id = $.cryptobox(3,9,5);
              signup.pass = _.htmlEscape(req.body.pass);
              signup.username = _.htmlEscape(req.body.username);
              signup.conversations = [];
              signup.profile_picture = "http://lorempixel.com/200/200/people/"

          if( signup.pass && signup.username){

                  accounts.findOne({username:signup.username }, function(e, o){
                    console.log(o);
                      if(o){

                          res.send(400); //this shouldn't happen anyway, but JIC
                      } else {
                          _.saltAndHash(signup.pass, function(hash){
                              signup.pass = hash;
                              accounts.insert(signup, function(){
                                req.session.user = signup;
                                res.send(o, 200);
                              }, res);
                          });
                      }

                  }, res);
          } else {

               res.send(400);
          }
      } else {

          res.send(400);
      }
  });
}
