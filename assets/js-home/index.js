//resolve





//get username


//check session


function request(url, type, data, resolve, reject){
  $.ajax({
     url: url,
     type: type,
     data: data,
     success: function(data) {
      resolve(data);
    },
   error:function(data){
     if(reject){
       reject();
     } else {
       alert('An error happened, dont worry just refresh');
     }
   }
  });
}

function pushMessage(from, content){
  var attributes = {style:{alternative:true}};
  if(data && data.user && from.username === data.user.username){
    attributes.style.alternative = false;
  }
  var html = "";
    html += "<div data-user='" + from.username + "' class='chat-convo--" + current_chatroom + " chat--item " + ((attributes.style.alternative) ? "chat--item-alternative" : "" ) + "'>";

    if(attributes.style.alternative){
      html += "<div class='chat--user-picture' style='background-image:url(" + from.profile_picture + ")'></div>";
    }

    html += "<div class='chat--content'>";
      if(attributes.style.alternative){
        html += "<div class='chat--username'>";
        html += from.username;
        html += "</div>";
      }
      html += "<div class='chat--text'>";
      html += content.text;
      html += "</div>";
    html += "</div>";

    html += "<div class='clear'></div>";
    html += "</div>";
  return html;
}

function generateFriend(friend, new_tool){
  var html = "";
      html += "<div class='friends--person " + ((friend.id === 'bot') ? "selected" : "not-bot") + "' " + ((new_tool) ? "data-new='yes' " : "")  + " data-id='" + friend.id + "'>";
      html += "<div class='friends--person-image' style='background-image:url(" + friend.profile_picture + ")'>";
      html += "</div>";
      html += "<div class='friends--person-name'>";
      html += (friend.name || friend.username);
      html += "</div>";
      html += "<div class='clear'></div>";
      html += "</div>";
  return html;
}

function sendMessage(){

  var text = $(".content--post input").val();
  $(".content--post input").val("");

  if(step_bot === 3 || log_bot === 3){
    text = text.replace(/./g, "*")
  }




  //send message to server
  if($(".friends--person.selected").data("id") === "bot"){
    $("#chatinterface").append(pushMessage(data.user, {text:text}));
    interceptBotText(text);

  } else {
    sendMessageBody(data.user, text);
  }
}

function bot(){
  return {id:"bot", profile_picture:"/img/profile/bot.png", username:"SwimBot"};
}

function getFriends(username){
  var sendData = {};
  var url = "";
  var new_tool = false;
  var html = "";

  if(username){
    url = "/userlist";
    new_tool = true;
    sendData = {username:username, check:"new"};
  } else {
    url = "/friendlist";
    sendData = {};
  }

  html += "<div class='friends--title'>People</div>";


  if(data.user.username === "guest"){
    html += generateFriend(bot());

    $(".sidebar--conversations").html(html);
  } else {
    request(url, "GET", sendData,
    function(friends){



        for(var i in friends){
          html += generateFriend(friends[i], new_tool);
        }

      $(".sidebar--conversations").html(html);
      if($(".friends--person:nth-child(2)").data("new") !== "yes"){
        $(".friends--person:nth-child(2)").click();
      }
    });
  }
}



function addFriend(id){
  //basically if you send a message, you are part of the friendlist
  request("/friendlist", "POST", {id:id},
    function(){

    }
  );
}

//registration script

var reg = {
  welcome:"Welcome to Swimchat!  A test system for webaware’s SWIM infrastrcuture.<br>To proceed to chat, please type <b>login</b> or <b>register</b>",
  username:"Cool, please enter your <b>username!</b>",
  wrong:"Sorry, this username is in use!",
  password:"Alright, please enter your <b>password!</b>",
  unknown:"I'm sorry, I don't understand!",
  checking:"Nice usernname, let me verify that no one has used this name before",
  registering:"Cool, let me register you in our system.",
  notfound:"I can't find this username in our system.",
  verify:"Let me check if you're in our system",
  wrongpassword:"Whoops, you entered the wrong password - try again!",
  loggin_in:"Checking your password now.",
  logged_in:"Password accepted, you're in!"

}

function reg_run(name){
  $("#chatinterface").append(pushMessage(bot(), {text:reg[name]}));
}

function startBot(){
  $("#chatinterface").html(""); //new
  $(".content-header-current-user").text("SwimBot");
  $(".content-header-last-time").text("Welcome to SwimChat!");
  reg_run("welcome");
}

function returnMessage(){

}

var step_bot = 1;
var log_bot = 1;
var username = "";

function interceptBotText(text){
  $(".content--post input").attr('type','text');

  if(text.indexOf("end") !== -1){
    step_bot = 1;
  } else {
    if(step_bot === 1 && log_bot === 1){
      if(text.indexOf("register") !== -1){
        reg_run("username");
        step_bot++;
      } else if(text.indexOf("login") !== -1){
        reg_run("username");
        log_bot++;
      } else {
        reg_run("unknown");
      }
    } else if(step_bot === 2 || log_bot === 2){
      if(log_bot === 2){
        reg_run("verify");
      } else {
        reg_run("checking");
      }
      verifyName(text, function(locked){
        if(log_bot === 2){
          if(locked){
            username = text;
            reg_run("password");
            $(".content--post input").attr('type','password');
            log_bot++
          } else {
            reg_run("notfound");
          }
        } else {
          if(locked){
            reg_run("wrong");
          } else {
            username = text;
            reg_run("password");
            $(".content--post input").attr('type','password');
            step_bot++
          }
        }
      });
    } else if(log_bot === 3){
      reg_run("loggin_in");
      completeLogin(text, function(){
            log_bot = 1;
            reg_run("logged_in");
          setTimeout(function(){
            startUser();
          }, 1000);


      });
    } else if(step_bot === 3){
      reg_run("registering");
      completeRegistration(text, function(){
          step_bot = 1;
          setTimeout(function(){
            startUser();
          }, 1000);

      });
    }
  }
}

function verifyName(text, resolve){
  request("/userlist", "GET", {username:text, check:"one"},
  function(locked){
    resolve(locked)
  });
}

function completeLogin(text, resolve){
  request("/login", "POST", {username:username, pass:text},
  function(user){
    //complete registration

    data.user = user;
    resolve();
  }, function(){
    reg_run("wrongpassword");
  });
}

function completeRegistration(text, resolve){
  request("/signup", "POST", {username:username, pass:text},
  function(user){
    //complete registration

    data.user = user;
    resolve()
  });
}

function clearChat(){
  $("#chatinterface").html("<div class='empty--clear-class'>Add a new friend or click on one that exists to talk!</div>");
}

function startUser(){
  clearChat();
  getFriends();

}

//login script
var data = {};

$(document).ready(function(){




  data = $("#data").data('content'); //GUEST: {user:{username:"guest"}};
  startUser();

  if(data.user.username === "guest"){

    startBot();
  }


  $(".sidebar--search input").keyup(function(e){
    var text = $(this).val();
    getFriends(text);
  });

  $(".content--post input").keydown(function(e){


    //send message
    if(e.keyCode === 13){

         sendMessage();
         e.preventDefault();
     }
  });
});

function setCurrentChatStatus(){

}

$(document).on("click", ".friends--person.not-bot", function(){
  $(".friends--person.not-bot").removeClass("selected");
  $(this).addClass("selected");
  $(".empty--clear-class").remove();

  var chatting_with = $(this).find(".friends--person-name").text().toLowerCase();
  var chatmembers = [chatting_with, data.user.username];

  chatmembers.sort();
  var chatroom = chatmembers[0] + "_" + chatmembers[1];
  current_chatroom = chatroom;

  $(".content-header-current-user").text(chatting_with);

  $(".chat--item").css('display', 'none');
  $(".chat-convo--" + current_chatroom).css('display', 'block');

  setCurrentChatStatus();
  joinChat(chatroom);

    if(activeChat.indexOf(chatroom) === -1){
      activeChat.push(chatroom);

      readMessages(chatroom);
    }






  if($(this).data("new") === "yes"){
    var id = $(this).data("id");
    addFriend(id);
  }



});

function doMessageLogic(message){
  if(	$( "#chatinterface .chat--item:last:visible").data("user") === message.body[0].from){
    $("#chatinterface .chat--item:last:visible").find(".chat--text").append("<br>" + message.body[1].body);
  } else {
    $("#chatinterface").append(pushMessage({username:message.body[0].from, profile_picture:"http://lorempixel.com/200/200/people/"}, {text:message.body[1].body}));
  }

  var date = new Date(message.body[2].time);
  var timesetup = date.getHours() + ":" + date.getMinutes() + " on " + date.getDate() + "/" + (date.getMonth() + 1);
  $(".content-header-last-time").text("Last Message Sent: " + timesetup); // set Time
  $("#chatinterface").scrollTop($("#chatinterface")[0].scrollHeight);


}

var current_chatroom = "";
var swim = swim.client;
var activeChat = [];

  function joinChat(chatroom) {
    var chat = "ws://messenger.swim.services/chat/" + chatroom;


      var command = recon({user: data.user.username});

      swim.sendCommand(chat, 'chat/enter', command);

  }

  function readMessages(chatroom) {

      swim.sync('ws://messenger.swim.services/chat/' + chatroom, 'chat/room', function(message){

          doMessageLogic(message);
      });

  }



  function sendMessageBody(from, text){
    var command = recon({from: from.username, body: text});
    swim.sendCommand('ws://messenger.swim.services/chat/' + current_chatroom, 'chat/post', command);
  }
